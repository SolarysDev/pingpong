const Discord = require('discord.js');

const client = new Discord.Client()
    .on("ready", () => {
        console.log(`ready! \nLogged in as ${client.user.username}. \nClient ID is ${client.user.id} \n-------------`)
    })
    .on("message", async message => {
        switch(message.content) {
            case "!ping":
                let time = Date.now();
                message.channel.send("ping?").then(m => {
                    let newtime = Date.now();
                    m.edit(`pong! that took ${newtime - time}ms. server heartbeat is ${client.ping}`);
                });
                break;
            case "!embedping":
                let time2 = Date.now();
                message.channel.send("ping?").then(m => {
                    let embed = new Discord.RichEmbed()
                        .setColor("DARK_PURPLE")
                        .setDescription(`pong! that took ${Date.now() - time2}ms`);
                    m.edit(embed);
                });
                break;
            case "!quit":
                await message.channel.send("ok bye");
                process.exit();

        }
    });
client.login(process.argv[2]);