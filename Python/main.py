import sys
import datetime

import discord
from discord.ext import commands
from discord.ext.commands import Bot

import asyncio

client = Bot(command_prefix="!")

@client.event
async def on_ready():
    print("bot is ready! \n----------------")
    
@client.command()
async def ping(context):
    time = round(datetime.datetime.now().microsecond / 1000)
    await context.send("ping?")
    newtime = round(datetime.datetime.now().microsecond / 1000)
    await context.send(f"pong! that took {newtime - time}ms")  

@client.command()
async def embedping(context):
    time = round(datetime.datetime.now().microsecond / 1000)
    await context.send("ping?")
    embed = discord.Embed(title="pong!", description="that took {}ms".format(round((datetime.datetime.now().microsecond / 1000)) - time))
    await context.send(embed=embed)

    

@client.command()
async def quit(context):
    await context.send("ok bye")
    sys.exit()

client.run(sys.argv[1])